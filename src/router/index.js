import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import MenuToggle from '@/components/menu/MenuToggle'
import NewsItemsList from '@/components/news/NewsItemsList'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/news',
      name: 'NewsItemsList',
      component: NewsItemsList
    },
    {
      path: '/menu',
      name: 'MenuToggle',
      component: MenuToggle
    }
  ]
})
